var express = require("express");
var request = require("request");
var bodyParser = require("body-parser");

const APP_TOKEN = "EAADrDTjiUSABAGPyERUR55insZBMj2Y4X7mXm1oUIoyI98vOsnhtZCRZB5NnrpX0ZASeaSeGaBhO0h2rMk68GrorZCZCx6SqSdxOEMUWLP698SnhkzwiZChMXdMsakIUTJZBwxobhEon35QytI11GFuhTYn1qmTR63m9LiLVKXLbuHJdGYpH0Up7";

var app = express();
app.use(bodyParser.json());

app.listen((process.env.PORT || 3000), function() {
    console.log('El servidor webhook esta escuchando!');
});

// Ruta de la pagina index
app.get("/", function(req, res) {
    res.send("Se ha desplegado de manera exitosa el Whizz ChatBot :D");
});

app.get('/webhook', function(req, res) {
    if (req.query['hub.verify_token'] === 'bot_messenger') {
        res.send(req.query['hub.challenge']);
    } else {
        res.send('Tu no tienes que entrar aqui');
    }
});

app.post('/webhook', function(req, res) {

    let body = req.body;

    if (body.object === 'page') {

        body.entry.forEach(function(entry) {

            entry.messaging.forEach(function(messagingEvent) {

                if (messagingEvent.message) {
                    receiveMessage(messagingEvent);
                }

            });

            let webhook_event = entry.messaging[0];
            console.log(webhook_event);

        });

        // Return a '200 OK' response to all events
        res.status(200).send('EVENT_RECEIVED');

    } else {
        res.sendStatus(404);
    }
});


function receiveMessage(event) {
    var senderID = event.sender.id;
    var messageText = event.message.text;

    evaluateMessage(senderID, messageText);
}

function evaluateMessage(recipientId, message) {
    var finalMessage = '';

    console.log(message);

    if (isContain(message, 'ayuda')) {
        finalMessage = 'Por el momento no te puedo ayudar';

    } else if (isContain(message, 'gato')) {

        sendMessageImage(recipientId);

    } else if (isContain(message, 'clima')) {

        getWeather(function(temperature) {

            message = getMessageWeather(temperature);
            sendMessageText(recipientId, message);

        });

    } else if (isContain(message, 'info')) {

        sendMessageTemplate(recipientId);

    } else {
        finalMessage = 'solo se repetir las cosas : ' + message;
    }
    sendMessageText(recipientId, finalMessage);
}

function sendMessageText(recipientId, message) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            text: message
        }
    };
    callSendAPI(messageData);
}

function sendMessageImage(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "image",
                payload: {
                    url: "http://i.imgur.com/SOFXhd6.jpg"
                }
            }
        }
    };
    callSendAPI(messageData);
}


function sendMessageTemplate(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "generic",
                    elements: [elemenTemplate()]
                }
            }
        }
    };
    callSendAPI(messageData);
}

function elemenTemplate() {
    return {
        title: "Eduardo Ismael",
        subtitle: "Desarrollado de Software en Código facilito",
        item_url: "https://www.facebook.com/codigofacilito/?fref=ts",
        image_url: "https://staticr1.blastingcdn.com/media/photogallery/2017/10/24/660x290/b_1433x630/revelada-la-primera-imagen-de-la-nueva-saga-de-dragon-ball-super-atomixvg_1651839.jpg",
        buttons: [buttonTemplate()],
    }
}

function buttonTemplate() {
    return {
        type: "web_url",
        url: "https://www.facebook.com/codigofacilito/?fref=ts",
        title: "Codigo Facilito"
    }
}

function callSendAPI(messageData) {
    request({
        uri: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {
            access_token: APP_TOKEN
        },
        method: 'POST',
        json: messageData
    }, function(error, response, data) {

        if (error) {
            console.log('No es posible enviar el mensaje');
        } else {
            console.log("El mensaje fue enviado");
        }

    });
}

function getMessageWeather(temperature) {
    if (temperature > 30)
        return "Nos encontramos a " + temperature + " Hay demaciado calor, te recomiendo que no salgas";
    return "Nos encontramos a " + temperature + " es un bonito día para salir";
}

function getWeather(callback) {
    request('http://api.geonames.org/findNearByWeatherJSON?lat=16.750000&lng=-93.116669&username=enjey11',
        function(error, response, data) {
            if (!error) {
                var response = JSON.parse(data);
                var temperature = response.weatherObservation.temperature;
                callback(temperature);
            }
        });
}

function isContain(sentence, word) {
    if (typeof sentence == 'undefined' || sentence.lenght <= 0) {
        return false;
    }
    return sentence.indexOf(word) > -1;
}

function receivedPostback(event) {
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;
    var timeOfMessage = event.timestamp;
    var payload = event.postback.payload;
    switch (payload) {
        case 'getstarted':
            console.log(payload);
            var msg = "Bienvenido {{user_first_name}}, Botych@t creado por Capsula! \n" +
                "Whizz Corporation\n";
            sendTextMessage(senderID, msg);
            break;
        case 'HELP_PAYLOAD':
            console.log(payload);
            var msg = "necesitas ayuda, que deseas saber?";
            sendTextMessage(senderID, msg);
            break;
        case 'CONTACT_INFO_PAYLOAD':
            console.log(payload);
            var msg = "Si deseas contactar con nosotros personalmente, nuestro correo es holawhizz.com.";
            sendTextMessage(senderID, msg);
            break;
        case '<GET_STARTED_PAYLOAD>':
            var msg = "Hola soy el asistente de Whizz.\n" + "Estás a un click de lavar tu automovil! Podrás elegir el tipo de lavado que deseas contratar, de forma fácil y segura.\n";
            sendTextMessage(senderID, msg);
            break;
        default:
            console.log(payload);
            var msg = "Hola soy el asistente de Whizz.\n" + "Estás a un click de lavar tu automovil! Podrás elegir el tipo de lavado que deseas contratar, de forma fácil y segura.\n";
            sendTextMessage(senderID, msg);
            break;
    }

}