'use strict';

const
    express = require("express"),
    request = require("request"),
    bodyParser = require("body-parser"),
    dotenv = require('dotenv').config(),
    emoji = require('node-emoji');

var
    fbMenu = require("./app/controllers/menu.js"),
    fbGreeting = require("./app/controllers/greeting.js"),
    fbGetStarted = require("./app/controllers/getStarted.js"),
    fbWhiteDomains = require("./app/controllers/whitelisted_domains.js"),
    fbVideo = require("./app/controllers/tplVideo.js"),
    fbUser = require("./app/controllers/getUser");

const
    APP_TOKEN = process.env.FB_ACCESS_TOKEN,
    VERIFY_TOKEN = process.env.FB_VERIFY_TOKEN,
    SERVER_URL = process.env.SERVER_URL;

var app = express();

app.use(bodyParser.json());
app.use(express.static('public'));

app.listen((process.env.PORT || 3000), function() {
    console.log('El servidor webhook esta escuchando!');
});

app.get("/", function(req, res) {
    res.send("Se ha desplegado de manera exitosa el Whizz ChatBot :D");
});

/**
 * SETUP BOT
 */
app.get('/setup', function(req, res) {
    // fbGetStarted.setupGetStartedButton(res);
    // fbGreeting.setupGreetingText(res);
    // fbMenu.setupPersistentMenu(res);
    fbWhiteDomains.whitelistedDomains(res);
    // setupGetStartedButton(res);
    // setupGreetingText(res);
});

/**
 * Sirve la ruta de opciones y establece los encabezados necesarios
 * Serve the options path and set required headers
 */
app.get('/options', function(req, res, next) {
    let referer = req.get('Referer');
    if (referer) {
        if (referer.indexOf('www.messenger.com') >= 0) {
            res.setHeader('X-Frame-Options', 'ALLOW-FROM https://www.messenger.com/');
        } else if (referer.indexOf('www.facebook.com') >= 0) {
            res.setHeader('X-Frame-Options', 'ALLOW-FROM https://www.facebook.com/');
        }
        res.sendFile('public/options.html', { root: __dirname });
    }
});

/**
 * Manejar la devolución de datos desde la vista web
 * Handle postback from webview
 */
app.get('/optionspostback', function(req, res) {
    let
        body = req.query,
        productos = body.productos.join(','),
        response = {
            "text": `estos son los escogidos ${productos} .`
        };

    res.status(200).send('Please close this window to return to the conversation thread.');
    receiveMessageWebview(body.psid, response);
});


// Usados para la verificacion
app.get("/webhook", function(req, res) {

    // Parse the query params
    var mode = req.query['hub.mode'];
    var token = req.query['hub.verify_token'];
    var challenge = req.query['hub.challenge'];

    // Comprueba si hay un token y un modo en la cadena de consulta de la solicitud
    if (mode && token) {

        // Comprueba el modo y el token enviado es correcto
        if (mode === 'subscribe' && token === VERIFY_TOKEN) {

            // Responde con el token de desafío de la solicitud
            console.log('WEBHOOK_VERIFIED');
            res.status(200).send(challenge);

        } else {
            // Responds with '403 Forbidden' if verify tokens do not match
            console.error("La verificacion ha fallado, porque los tokens no coinciden");
            res.sendStatus(403);
        }
    }
});

/**
 * Method: Post
 * Function: webhook
 */
app.post('/webhook', function(req, res) {
    var body = req.body;
    if (body.object === 'page') {
        body.entry.forEach((entry) => {
            if (entry.messaging) {
                entry.messaging.forEach((messagingEvent) => {
                    if (messagingEvent.message) {
                        receiveMessage(messagingEvent);
                        // fbUser.getUser(messagingEvent.sender.id, APP_TOKEN);
                        // console.log(messagingEvent.message.quick_reply);
                    } else if (messagingEvent.postback) {
                        asyncCall(messagingEvent);
                    }
                    // let webhook_event = entry.messaging[0];
                    // console.log(webhook_event);
                });
            }
        });

        // Returns a '200 OK' response to all requests
        res.status(200).send('EVENT_RECEIVED');
    } else {
        // Returns a '404 Not Found' if event is not from a page subscription
        res.sendStatus(404);
    }

});

/**
 *
 *
 * @param {*} event
 */
function receiveMessage(event) {
    var senderID = event.sender.id;
    var messageText = event.message.text;

    evaluateMessage(senderID, messageText);
}

/**
 *
 */
function receiveMessageWebview(senderID, messageText) {
    var senderID = senderID;
    var messageText = messageText.text;

    evaluateMessage(senderID, messageText);
}

/**
 *
 *
 * @param {*} recipientId
 * @param {*} message
 */
function evaluateMessage(recipientId, message) {
    var finalMessage = '';

    if (isContain(message, 'ayuda')) {
        finalMessage = 'Por el momento no te puedo ayudar';
    } else {
        finalMessage = 'solo se repetir las cosas : ' + message;
    }
    sendMessageText(recipientId, finalMessage);
}

/**
 *
 *
 * @param {*} recipientId
 * @param {*} message
 */
function sendMessageText(recipientId, message) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            text: message
        }
    };
    callSendAPI(messageData);
}

/**
 *
 *
 * @param {*} sentence
 * @param {*} word
 * @returns
 */
function isContain(sentence, word) {
    if (typeof sentence == 'undefined' || sentence.lenght <= 0) {
        return false;
    }
    return sentence.indexOf(word) > -1;
}

/**
 *
 *
 * @param {*} messageData
 */
function callSendAPI(messageData) {
    request({
        uri: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {
            access_token: APP_TOKEN
        },
        method: 'POST',
        json: messageData
    }, function(error, response, data) {

        if (error) {
            console.log('MSG: ERROR');
        } else {
            console.log("MSG: OK");
        }

    });
}

/**
 * Evento: Postback
 *
 * @param {*} event
 */
function processPostback(event) {
    var senderID = event.sender.id;
    var payload = event.postback.payload;
    switch (payload) {
        case 'GET_STARTED':
            console.log(payload);
            var msg = "Bienvenido, Botych@t creado por Capsula! \n" +
                "Whizz Corporation\n";
            sendMessageText(senderID, msg);
            break;
        case '<GET_STARTED_PAYLOAD>':
            console.log(payload);
            getStartedPayload(senderID);
            break;
        case 'CALL_CATEGORIAS':
            console.log(payload);
            var msg = "Vamos a solicitar un servicio. Por favor selecciona una categoría:";
            sendMessageText(senderID, msg);
            tplListCategorias(senderID);
            // tplListServicio(senderID);
            break;
        case 'CALL_SERVICIO':
            console.log(payload);
            var msg = "Vamos a solicitar un servicio de lavado ecoamigable. Por favor selecciona un servicio:";
            sendMessageText(senderID, msg);
            tplListServicio(senderID);
            break;
        case 'CONTACT_INFO_PAYLOAD':
            console.log(payload);
            var msg = "Si deseas contactar con nosotros personalmente, nuestro correo es holawhizz.com.";
            sendMessageText(senderID, msg);
            break;
        case '<SERVICIO_SELECCIONADO>':
            console.log(payload);
            selectAutomovil(senderID);
            break;
        case '<CATEGORIAS_DETALLE>':
            console.log(payload);
            setRoomPreferences(senderID);
            break;
        default:
            console.log(payload);
            var msg = "Hola soy el asistente de Whizz.\n" + "Estás a un click de lavar tu automovil! Podrás elegir el tipo de lavado que deseas contratar, de forma fácil y segura.\n";
            sendMessageText(senderID, msg);
            break;
    }

}

/**
 * Evento: Postback
 * Payload: <GET_STARTED_PAYLOAD>
 *
 * @param {*} senderID
 */
function getStartedPayload(senderID) {
    request({
        url: "https://graph.facebook.com/v2.6/" + senderID,
        qs: {
            access_token: APP_TOKEN,
            fields: "first_name,last_name",
        },
        method: "GET"
    }, function(error, response, body) {
        var greeting = "";
        if (error) {
            console.log("Error getting user's name: " + error);
        } else {
            var bodyObj = JSON.parse(body);
            var firstName = bodyObj.first_name;
            var lastName = bodyObj.last_name;
            var user_name = "🤝 Hola " + firstName + " " + lastName + ",\n";
        }

        var message = user_name + "Bienvenido a Whizz 👍, Somos la primera empresa de lavado ecológico. ¿como te puedo ayudar?";

        textWelcome(senderID, message);
    });
}

/**
 *
 *
 * @param {*} senderID
 */
function msgCompraNueva(senderID) {
    var messageData = {
        recipient: {
            id: senderID
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: 'media',
                    elements: [{
                        media_type: "video",
                        url: "https://www.facebook.com/WhizzPeru/videos/354312825004338/"
                    }]
                }
            }
        }
    };
    callSendAPI(messageData);
}

/**
 *
 *
 * @param {*} senderID
 * @param {*} msg
 */
function textWelcome(senderID, msg) {
    var messageData = {
        recipient: {
            id: senderID
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "button",
                    text: msg,
                    buttons: [
                        buttonTplPostback('👍  Solicitar servicio!', 'CALL_CATEGORIAS')
                    ]
                }
            }
        }
    };
    callSendAPI(messageData);
}

/**
 * Evento: Postback
 * Payload: CALL_CATEGORIAS
 *
 * @param {*} senderID
 */
function tplListCategorias(senderID) {
    let
        title1 = "ECO-LAVADOS",
        img1 = "http://holacapsula.com/bot/imgs/categorias/eco-lavados.png",
        title2 = "DELIVERY DE BATERIAS",
        img2 = "http://holacapsula.com/bot/imgs/categorias/delivery-bateria.png",
        title3 = "SEGUROS Y SOAT",
        img3 = "http://holacapsula.com/bot/imgs/categorias/seguros-y-soat.png";

    var messageData = {
        recipient: {
            id: senderID
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: 'generic',
                    elements: [tplCategoria(title1, img1), tplCategoria(title2, img2), tplCategoria(title3, img3)]
                }
            }
        }
    };

    callSendAPI(messageData);
}

/**
 * @param {*} title
 * @param {*} img
 * @returns
 */
function tplCategoria(title, img) {
    return {
        title: title,
        image_url: img,
        buttons: [{
            type: "web_url",
            url: SERVER_URL + "/options",
            title: "👉 Seleccionar",
            webview_height_ratio: "tall",
            webview_share_button: "hide",
            messenger_extensions: true
        }]
    };
}

/**
 * Evento: Postback
 * Payload: CALL_SERVICIO
 * @param {*} senderID
 */
function tplListServicio(senderID) {
    var title1 = "BÁSICO";
    var img1 = "http://holacapsula.com/bot/imgs/servicios/basico.png";
    var pd1 = "<BASICO_DETALLES>";
    var ps1 = "<SERVICIO_SELECCIONADO>";
    var title2 = "COMPLETO";
    var img2 = "http://holacapsula.com/bot/imgs/servicios/completo.png";
    var pd2 = "<COMPLETO_DETALLES>";
    var ps2 = "<SERVICIO_SELECCIONADO>";
    var title3 = "LAVADO DE SALÓN";
    var img3 = "http://holacapsula.com/bot/imgs/servicios/salon.png";
    var pd3 = "<SALON_DETALLES>";
    var ps3 = "<SERVICIO_SELECCIONADO>";
    var title4 = "TRATAMIENTO DE PINTURA";
    var img4 = "http://holacapsula.com/bot/imgs/servicios/pintura.png";
    var pd4 = "<PINTURA_DETALLES>";
    var ps4 = "<SERVICIO_SELECCIONADO>";
    var title5 = "DESCONTAMINACIÓN DE A/C";
    var img5 = "http://holacapsula.com/bot/imgs/servicios/descontaminacion-a-c.png";
    var pd5 = "<DESCONTAMINACION_DETALLES>";
    var ps5 = "<DESCONTAMINACION_SELECCIONADO>";
    var messageData = {
        recipient: {
            id: senderID
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: 'generic',
                    elements: [tplServicios(title1, img1, pd1, ps1), tplServicios(title2, img2, pd2, ps2), tplServicios(title3, img3, pd3, ps3), tplServicios(title4, img4, pd4, ps4), tplServicios(title5, img5, pd5, ps5)]
                }
            }
        }
    };

    callSendAPI(messageData);
}

/**
 *
 *
 * @param {*} title
 * @param {*} img
 * @param {*} payload_var_detalles
 * @param {*} payload_var_seleccionar
 * @returns
 */
function tplServicios(title, img, payload_var_detalles, payload_var_seleccionar) {
    return {
        title: title,
        subtitle: "Lavado de carrocerias + limpieza de cristales y neumáticos.",
        image_url: img,
        buttons: [
            buttonTplPostback('👉 Ver detalles', payload_var_detalles),
            buttonTplPostback('🛵 Seleccionar servicio!', payload_var_seleccionar),
        ]
    };
}

/**
 * Evento: Postback
 * Payload: ID_SERVICIO
 * @param {*} senderID
 */
function selectAutomovil(senderID) {
    request({
        url: "https://graph.facebook.com/v2.6/" + senderID,
        qs: {
            access_token: APP_TOKEN,
            fields: "first_name,last_name",
        },
        method: "GET"
    }, function(error, response, body) {
        var greeting = "";
        if (error) {
            console.log("Error getting user's name: " + error);
        } else {
            var bodyObj = JSON.parse(body);
            var firstName = bodyObj.first_name;
            var lastName = bodyObj.last_name;
            var user_name = "🤝 " + firstName + " " + lastName + ",\n";
        }
        var message = user_name + "¿qué tipo de automovil tiene usted?";
        textAutomovil(senderID, message);
    });
}

/**
 *
 *
 * @param {*} senderID
 * @param {*} msg
 */
function textAutomovil(senderID, msg) {
    var messageData = {
        recipient: {
            id: senderID
        },
        message: {
            text: msg,
            quick_replies: [{
                content_type: "text",
                title: "Auto",
                payload: "AUTO",
                image_url: "http://holacapsula.com/bot/icons/auto.png"
            }, {
                content_type: "text",
                title: "SUV",
                payload: "SUV",
                image_url: "http://holacapsula.com/bot/icons/suv.png"
            }, {
                content_type: "text",
                title: "Camioneta",
                payload: "CAMIONETA",
                image_url: "http://holacapsula.com/bot/icons/camioneta.png"
            }]
        }
    };
    callSendAPI(messageData);
}

/**
 * Definir la plantilla y la vista web
 * Define the template and webview
 * @param {*} senderID
 * @returns
 */
function setRoomPreferences(senderID) {
    var messageData = {
        recipient: {
            id: senderID
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "button",
                    text: "listado",
                    buttons: [{
                        type: "web_url",
                        url: SERVER_URL + "/options",
                        title: "Listado Categoría",
                        webview_height_ratio: "tall",
                        webview_share_button: "hide",
                        messenger_extensions: true
                    }, {
                        type: "postback",
                        title: "Trigger Postback",
                        payload: "DEVELOPED_DEFINED_PAYLOAD"
                    }, {
                        type: "phone_number",
                        title: "Call Phone Number",
                        payload: "+16505551234"
                    }]
                }
            }
        }
    };

    callSendAPI(messageData);
}

/**
 * Evento: Postback
 * Payload: SEND_LOCATION
 *
 */
function enviarUbicacion() {}

/**
 *
 *
 * @param {*} senderID
 */
function tplServiciossss(senderID) {
    var messageVideo = {
        recipient: {
            id: senderID
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "media",
                    elements: [elementTemplateVideo("https://scontent.flim15-2.fna.fbcdn.net/v/t1.0-9/29594836_433116310457322_6612587768834960370_n.jpg?_nc_cat=0&oh=942821a87e49ac9530f4739cc684227a&oe=5BFFE440"), elementTemplateVideo("https://scontent.flim15-2.fna.fbcdn.net/v/t1.0-9/29594836_433116310457322_6612587768834960370_n.jpg?_nc_cat=0&oh=942821a87e49ac9530f4739cc684227a&oe=5BFFE440")]
                }
            }
        }
    };
    callSendAPI(messageVideo);
}

/**
 *
 *
 * @param {*} url
 * @returns
 */
function elementTemplateVideo(url) {
    return {
        media_type: "<image>",
        attachment_id: url
    }
}

/**
 *
 *
 * @param {*} img
 * @returns
 */
function elementTemplate(img) {
    return {
        title: "Joseph Esteban Carrasco",
        subtitle: "Programador freelance & Youtuber",
        image_url: img,
        buttons: [
            buttonTplPostback('👍 Comprar aqui!', 'COMPRAR_AQUI')
        ]
    }
}

/**
 *
 *
 * @param {*} title
 * @param {*} url
 * @returns
 */
function buttonTemplate(title, url) {
    return {
        type: 'web_url',
        url: url,
        title: title
    }
}

/**
 *
 *
 * @param {*} title
 * @param {*} payload_var
 * @returns
 */
function buttonTplPostback(title, payload_var) {
    return {
        type: "postback",
        title: title,
        payload: payload_var
    };
}

/**
 * EVENTO: postback
 * al iniciar conversación
 */
function resolveAfter2Seconds(msgevent) {
    return new Promise(resolve => {
        setTimeout(() => {
            processPostback(msgevent);
            resolve('resolved');
        }, 2000);
    });
}

async function asyncCall(msgevent) {
    if (msgevent.postback.payload === "<GET_STARTED_PAYLOAD>") {
        msgCompraNueva(msgevent.sender.id);
    }
    console.log('calling');
    var result = await resolveAfter2Seconds(msgevent);
    console.log(result);
}

/**
 */
function setupGetStartedButton(res) {
    var messageData = {
        "get_started": {
            "payload": "<GET_STARTED_PAYLOAD>"
        }
    };
    // Start the request
    request({
            url: "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=" + APP_TOKEN,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            form: messageData
        },
        function(error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send(body);

            } else {
                res.send(body);
            }
        });
}

function setupGreetingText(res) {
    var messageData = {
        "greeting": [{
            "locale": "default",
            "text": "Bienvenido {{user_first_name}}! Somos Whizz, empresa de lavado ecológico."
        }]
    };
    request({
            url: 'https://graph.facebook.com/v2.6/me/messenger_profile?access_token=' + APP_TOKEN,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            form: messageData
        },
        function(error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send(body);
            } else {
                res.send(body);
            }
        });

}