'use strict';

const
    request = require("request"),
    config = require("config");

module.exports = {

    getUser: function(senderID, APP_TOKEN) {
        request({
            url: "https://graph.facebook.com/" + senderID,
            qs: {
                access_token: APP_TOKEN,
                fields: "first_name,last_name,profile_pic",
            },
            method: "GET"
        }, function(error, response, body) {
            var greeting = "";
            if (error) {
                // console.log("Error getting user's name: " + error);
                let code_error = "Error getting user's name:" + error;
                return code_error;
            } else {
                var bodyObj = JSON.parse(body);
                // var firstName = bodyObj.first_name;
                // var lastName = bodyObj.last_name;
                // var user_name = "🤝 Hola " + firstName + " " + lastName + ",\n";
                // console.log(bodyObj);
                return bodyObj;
            }

            // var message = user_name + "Bienvenido a Whizz 👍, Somos la primera empresa de lavado ecológico. ¿como te puedo ayudar?";

            // textWelcome(senderID, message);
        });
    }

};