'use strict';

const
    request = require("request"),
    dotenv = require('dotenv').config(),
    SERVER_URL = process.env.SERVER_URL,
    APP_TOKEN = process.env.FB_ACCESS_TOKEN;

module.exports = {

    whitelistedDomains: function(res) {
        var messageData = {
            "whitelisted_domains": [
                SERVER_URL
            ]
        };
        // Start the request
        request({
                url: 'https://graph.facebook.com/v2.6/me/messenger_profile?access_token=' + APP_TOKEN,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                form: messageData
            },
            function(error, response, body) {
                if (!error && response.statusCode === 200) {
                    res.send(body);
                } else {
                    res.send(body);
                }
            });
    }

};