'use strict';

const request = require("request");
const config = require("config");

const APP_TOKEN = config.get('fb_access_token');

module.exports = {

    setupGetStartedButton: function(res) {
        var messageData = {
            "get_started": {
                "payload": "<GET_STARTED_PAYLOAD>"
            }
        };
        // Start the request
        request({
                url: 'https://graph.facebook.com/v2.6/me/messenger_profile?access_token=' + APP_TOKEN,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                form: messageData
            },
            function(error, response, body) {
                if (!error && response.statusCode === 200) {
                    res.send(body);
                } else {
                    res.send(body);
                }
            });
    }

};