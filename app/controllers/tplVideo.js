module.exports = {

    tplVideo: function(senderID, urlVideo) {
        var messageData = {
            recipient: {
                id: senderID
            },
            message: {
                attachment: {
                    type: "template",
                    payload: {
                        template_type: 'media',
                        elements: [{
                            media_type: "video",
                            url: urlVideo,
                            buttons: [{
                                type: "postback",
                                title: '👍 Comprar aqui!',
                                payload: 'COMPRAR_AQUI'
                            }]
                        }]
                    }
                }
            }
        };
    }

};