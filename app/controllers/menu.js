'use strict';

const
    request = require("request"),
    dotenv = require('dotenv').config(),
    APP_TOKEN = process.env.FB_ACCESS_TOKEN;

module.exports = {

    setupPersistentMenu: function(res) {
        var messageData = {
            "persistent_menu": [{
                "locale": "default",
                "composer_input_disabled": false,
                "call_to_actions": [{
                    "title": "👨‍💻 Mi cuenta",
                    "type": "nested",
                    "call_to_actions": [{
                        "title": "Mis datos",
                        "type": "postback",
                        "payload": "ACCOUNT_MY_DATA"
                    }, {
                        "title": "Mis pedidos",
                        "type": "postback",
                        "payload": "ACCOUNT_MY_ORDERS"
                    }]
                }, {
                    "title": "🎫 Servicios",
                    "type": "postback",
                    "payload": "CATEGORIAS"
                }, {
                    "title": "🏠 Whizz",
                    "type": "nested",
                    "call_to_actions": [{
                        "title": "FAQ o Ayuda",
                        "type": "postback",
                        "payload": "HELP_PAYLOAD"
                    }, {
                        "title": "Contáctanos",
                        "type": "postback",
                        "payload": "CONTACT_INFO_PAYLOAD"
                    }]
                }]
            }]
        };
        // Start the request
        request({
                url: "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=" + APP_TOKEN,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                form: messageData
            },
            function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    // Print out the response body
                    res.send(body);
                } else {
                    // TODO: Handle errors
                    res.send(body);
                }
            });
    }

};